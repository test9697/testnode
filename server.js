const express = require('express')
const cors = require('cors');

const app = express()


const routerPerson = require('./routes/person')

app.use(cors('*'));
app.use(express.json());

//add the router
app.use('/person',routerPerson)

app.listen(3000,'0.0.0.0',()=>{
    console.log('server started on port 3000');
})