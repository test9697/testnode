const { Router, response, request } = require("express");
const express = require("express");
const db = require("../db");
const utils = require("../utils");

const router = express.Router();

router.get("/", (request, response) => {
  // const query  = 'select  id , firstName, lastName, email, password from person';
  const query = "select  id , firstName, lastName from person";
  db.pool.execute(query, (error, persons) => {
    response.send(utils.createResult(error, persons));
  });
});

router.post("/", (request, response) => {
  const { firstName, lastName, email, password } = request.body;
  const query = `insert into person (firstName, lastName, email, password) values (?,?,?,?)`;
  db.pool.execute(
    query,
    [firstName, lastName, email, password],
    (error, persons) => {
      response.send(utils.createResult(error, persons));
    }
  );
});
// router.post('/',(request,response)=>{

// })

router.put("/update/:id", (request, response) => {
  const { id } = request.params;
  const { lastName } = request.body;

  const query = `update person set lastName=? where id=?`;
  db.pool.execute(query, [lastName, id], (error, persons) => {
    response.send(utils.createResult(error, persons));
  });
});

router.delete("/delete/:id", (request, response) => {
  const { id } = request.params;
  const query = `delete from person where id=?`;
  db.pool.execute(query, [id], (error, persons) => {
    response.send(utils.createResult(error, persons));
  });
});

module.exports = router;
